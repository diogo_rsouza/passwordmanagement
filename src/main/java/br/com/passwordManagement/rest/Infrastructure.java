package br.com.passwordManagement.rest;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/infrastructure")
public class Infrastructure {
	@GET 
	@Produces("text/plain")
	@Path("/ping")
	public String ping(){
		Date date = new Date();
		return date.toString();
	}
}
